#include <stdio.h>

int bar() {
    // This function will be used later on
    return 0;
}

void baz() {
    // This is a useful comment about the purpose of this function
    printf("Hello, everybody!\n);
}

int main() {
    printf("Hello, world!\n");
    return 0;
}

