#                                                      TP noté Git - Durée : 3 heures

Instructions :
* Nous allons utiliser le dépôt disponible au lien suivant :

[https://forge.univ-lyon1.fr/sebastien.gadrat/tuto-git-fusion.git](https://forge.univ-lyon1.fr/sebastien.gadrat/tuto-git-fusion.git)   

* Faites une copie de ce dépôt dans votre espace personnel (un "fork"), puis cloner ce dépôt localement.

* Suivez les instructions, et réalisez les actions dans l'ordre indiqué.

* Vous avez le droit d'utiliser les documents de cours ainsi que les pages de manuel de Git.

* Toutes les commandes Git utilisées seront données dans le compte-rendu (ou insérées dans le README.md), et effectuées par la suite.

* La note finale tiendra compte de vos réponses, mais aussi de la qualité de vos messages de commit.


## I. Prise en main du dépôt (30 minutes – 4 points)

On travaillera dans un dépôt local.

1. Sur quelle branche êtes-vous ?

   We are currently in the main branch. 

2. Combien de branches contient ce dépôt ?

   There are 3 branches : develop, feature, main

3. Pourquoi certaines branches apparaissent-elles en rouge ?

   They appear in red because they are on the remote server.

4. À quoi sert le fichier `.gitignore` ? Que contient-il ?

   The .gitignore file is used to list the files and directories that will be ignored during the commit.

   There is 2 file : example and example.o

5. Créez une nouvelle branche `test` à partir de la branche `main`.

   I used the command "git branch test".

6. Ajoutez une fonction `foobar()` au code C qui renvoie un entier. Elle ressemblera à ça :

I add the following code in the example.c file.

```
int foobar() {
	return 1;
}
```
Et on ajoutera cela dans la fonction main() :
```    
int test = foobar() ;
printf("foobar returns :%d\n", test);
```
La compilation du code se fera comme suit, en utilisant le fichier `Makefile` :

    $ make

Ou directement en utilisant la commande :

    $ gcc -o example example.c

7. Faites un commit avec vos modifications.

   I used the command `git add .`

   I used the command `git commit -m "modifiy example.c`

8. Poussez votre branche sur le dépôt distant.

   I used the command `git push https://forge.univ-lyon1.fr/p1401409/tuto-git-fusion.git`

## II. Fusion de branches avec conflit (30 minutes – 4 points)

1. Revenez sur la branche `main`.

   I was already on the main branch (i shoud've been in the test branch), but if i was on the test branch, I shoud've used `git checkout main`.

2. Les deux stratégies principales de fusion de branches sont appelées par avance rapide, ou encore "fast-forward" (anglais), et récursive ou "non-fast-forward" (anglais). Quelle est la principale différence entre ces deux stratégies de fusion (qui représentent les deux principalement utilisées).

   The difference between "fast-forward" and "non-fast-forward" is about the way Git manage the commit history when he merge : 

   - fast-forward : the history of the merged branch will become the history of the current branch
   - non-fast-forward : keep tje history of both branch

3. Laquelle est la plus communément utilisée dans la gestion d’un projet collaboratif ? Pourquoi ?

   The most-used strategy is the non-fast-forward because it keep the history of each branch. It's usefull when you work in a project with other people, because we can access the history of all the modification that has been done in each branch, by everyone. 

4. Fusionnez la branche `develop` dans la branche `main` en utilisant la stratégie de fusion dite "non-fast-forward".

​		I used the command `git merge --no-ff origin/develop`

5. Il y a conflit… Quelle commande puis-je faire pour annuler la fusion en cours ?

I used the command `git merge --abort`

6. Corrigez le conflit de fusion en modifiant le code de manière appropriée. On suppose ici que c’est le code de la branche develop qui est celui à conserver.

I just copy-pasted the code from the example.c file in the develop branch into the main branch. This is not the right way to do it, I shoud've used the `git diff example.c` command in order to see the difference between the two files. 

1. Faites un commit avec vos modifications.

   I used `git commit -m "correction of example.c"`

2. Poussez votre branche sur le dépôt distant.

   I used `git push https://forge.univ-lyon1.fr/p1401409/tuto-git-fusion.git `


## III. Fusion de branches avec stratégie octopus (1 heure, 6 points)

Maintenant que vous avez compris comment fusionner des branches avec les stratégies de fusion standard, nous allons voir comment utiliser la stratégie octopus pour fusionner plusieurs branches en une seule opération.

1. Récupérez les dernières modifications

Comme précédemment, vous devez vous assurer que vous êtes à jour avec les dernières modifications des branches `develop` et `feature`. Pour cela, effectuez les commandes suivantes :
```
$ git switch develop
$ git pull
$ git switch feature
$ git pull
```
* Que fait la commande `git pull` ? Décrire son fonctionnement.

  The `git pull` command allows you to get the modification from a remote repository and merge them with our current branch. It's the combination of `git fetch` and `git merge`. 

* Pourquoi exécuter ces commandes ?

  This is a very handy command to synchronize your local repository with changes made by other team members.

2. Fusionner les branches avec octopus

Maintenant que vous êtes à jour, vous pouvez fusionner les branches avec la stratégie octopus. Cette stratégie permet de fusionner plusieurs branches en une seule opération.
```
$ git switch main
$ git merge develop feature
```
3. Résoudre les conflits

Si des conflits surgissent lors de la fusion, vous devrez les résoudre en utilisant les mêmes techniques que celles décrites précédemment.
La version à conserver est celle contenant les fonctions `bar()` et `baz()`.

I used `git diff example.c` in order to see the conflict. Then, i opened visual studio code in order to resolve the merge conflit. I choose to keep the code from the feature branch, as it was asked.

4. Tester la compilation

There is a few errors when I compile. 

Une fois que la fusion est terminée, vous devez tester que le code compile correctement.
Si le code compile correctement, vous avez terminé la partie III de l'examen. Cependant, dans ce cas précis, vous allez constater que le code ne compile pas. En effet, la fusion avec la branche feature a introduit un bug. Il va donc falloir utiliser `git bisect` pour trouver le commit responsable de l'introduction de ce bug.

## III. Utilisation de Git bisect pour déterminer le commit introduisant le bug (1 heure, 6 points)

1. Comment peut-on afficher l’historique de la branche courante ? Qu'est-ce que l'identifiant d'un commit ? Où le trouve-t-on ?

   we can use `git log` . The commit ID is a "hash" , a string that identify the commit. It's unique for each commit. We can find in the output of the `git log` command. 

2. Comment peut-on visualiser les différences entre deux branches ? Entre deux commits ? Entre deux branches entre le dépôt local et le dépôt distant ?

   We can use `git diff branche1 branche2` to see the difference between both branches. We can also use `git diff id_commit1 id_commit2` to see the difference between two commits. We can use `git diff localbranch remotebranch` to see the difference between a local and a remote branch

3. On va utiliser `git bisect` pour déterminer le commit introduisant le bug.

4. Exécutez la commande `git bisect start`.

5. Exécutez la commande `git bisect bad HEAD` pour marquer le commit actuel comme étant mauvais.

6. Trouvez un commit connu pour être bon, par exemple le premier commit dans la branche main, et de l'exécuter en utilisant la commande `git bisect good <commit-id>`.

   I used ` git bisect good def810be71b6c085aa98c93d610163c8c525c931`

7. Vérifiez si le commit actuel est bon ou mauvais en compilant le code.

   There is still an error, but less than before 

8. Exécutez `git bisect good` ou `git bisect bad` en fonction du résultat de l'étape précédente pour marquer le commit courant comme étant bon ou mauvais.

9. Répétez les étapes 6 et 7 jusqu'à ce que le commit introduisant le bug soit identifié (processus itératif).

10. Terminez le processus de bisect en utilisant la commande `git bisect reset`.

11. Placez-vous sur le commit qui a introduit le bug en créant une nouvelle branche, appelée `bug1`, corrigez le bug sur cette nouvelle branche en suivant la proposition donnée lors de la tentative de compilation, puis effectuer une fusion sur main pour corriger ce bug.

    I used `git checkout e12271b` to go on the right commit, and the i used `git switch -c bug1` to create a new branch. It appears that I checkout on the wrong commit.  I misunderstand git bisect. I will restart the process in order to understand the git bisect, so maybe i won't be able to finish this TP nicely. 

12. De nouveau sur la branche main, compiler le code. Que se passe-t-il ?

13. Répétez les étapes précédentes pour corriger ce second bug (en créant une nouvelle branche `bug2`), et obtenir un code qui compile.

14. Annulez ce dernier commit correcteur.

15. Nous allons voir ici une autre manière de porter le commit qui corrige le bug sur la branche main : on utilisera ici la commande `git cherry-pick` (qui agira comme un patch). Pour cela, placez-vous sur main, et, avec cette commande, importer et jouer le commit correcteur sur la branche `main`.
